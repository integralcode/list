# Integral Code's Curated List.

This repo contain useful link for almost every aspect of web development.

## Contributing
Feel free to contribute. Pull request are highly appreciated.

---

## Coding Style Guide
- https://github.com/airbnb/css
- http://seesparkbox.com/foundry/lets_write_beautiful_css_comments

## Curated List
- Awesome Lists - https://github.com/sindresorhus/awesome

## Graphic
- https://www.codepolitan.com/kumpulan-situs-penyedia-gambar-vector-terbaik/

## Inspiration  
- http://www.awwwards.com/
- https://www.behance.net/
- https://dribbble.com/
- http://designdisease.com/45-inspiring-travel-and-tourism-website-designs/

## Image Compressor
- http://optipng.sourceforge.net/
- http://jpegclub.org/jpegtran/
- https://github.com/svg/svgo

## Learning & Practice
Some resource to learn how-to code.

- www.codecademy.com (online judge)
- www.codecombat.com (interactive coding)
- www.codefights.com (code againts bots)
- www.freecodecamp.com (online judge)
- www.sololearn.com (quiz & materi)

## News
- Panda - http://usepanda.com/ (News Feed, contain any site below.)
- Designer News - https://www.designernews.co/
- Front End Front - https://frontendfront.com/
- Hacker News - https://news.ycombinator.com/
- Smashing Magazine - https://www.smashingmagazine.com/

## Testing
- https://medium.com/planet-arkency/frontend-is-a-separate-application-8988a3ce70ad

## Paradigm
- http://blog.alexdevero.com/web-design-process-pt1-discovery/